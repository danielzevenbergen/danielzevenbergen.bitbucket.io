var searchData=
[
  ['deactivate_0',['deactivate',['../classclosedloop_1_1_closed_loop.html#a268732d1885b24e1e9d8d81a9a921c5e',1,'closedloop.ClosedLoop.deactivate(self)'],['../classclosedloop_1_1_closed_loop.html#a268732d1885b24e1e9d8d81a9a921c5e',1,'closedloop.ClosedLoop.deactivate(self)'],['../classclosedloop_1_1_closed_loop.html#a268732d1885b24e1e9d8d81a9a921c5e',1,'closedloop.ClosedLoop.deactivate(self)']]],
  ['delta_1',['delta',['../classencoder_1_1_encoder.html#ad017c0a5f382fe0dac6ed8920ce90635',1,'encoder::Encoder']]],
  ['derivative_2',['derivative',['../classclosedloop_1_1_closed_loop.html#aa508e71d17db2f1e3da24fc26d127ee4',1,'closedloop::ClosedLoop']]],
  ['diff_3',['diff',['../classencoder_1_1_encoder.html#a10b620569fd5a3faa62af2dab593c213',1,'encoder::Encoder']]],
  ['disable_4',['disable',['../class_d_r_v8847_1_1_d_r_v8847.html#acd9dbef9212b3014eab18a57a6e0f13a',1,'DRV8847.DRV8847.disable(self)'],['../class_d_r_v8847_1_1_d_r_v8847.html#acd9dbef9212b3014eab18a57a6e0f13a',1,'DRV8847.DRV8847.disable(self)']]],
  ['drv8847_5',['DRV8847',['../class_d_r_v8847_1_1_d_r_v8847.html',1,'DRV8847']]],
  ['drv8847_2epy_6',['DRV8847.py',['../_lab_013_2_d_r_v8847_8py.html',1,'(Global Namespace)'],['../_lab_014_2_d_r_v8847_8py.html',1,'(Global Namespace)']]],
  ['dtask_7',['Dtask',['../_lab_013_2task_data_8py.html#a24ccfe31d20546ee8eaaad000aa14018',1,'taskData.Dtask(datarecord, dataFlag, datapoints, timeData, positionData, deltaData, pos, delta, timeout, finalindex, period)'],['../_lab_016_2task_data_8py.html#a880cdc07d62a00e830e96fccb0cd9b4e',1,'taskData.Dtask(datarecord, dataFlag, datapoints, timeData, channel1, channel2, channel3, channel4, rollang, pitchang, rollvel, pitchvel, ballpos_X, ballpos_Y, ballvel_X, ballvel_Y, dutycycle1, dutycycle2, timeout, finalindex, period)']]]
];
