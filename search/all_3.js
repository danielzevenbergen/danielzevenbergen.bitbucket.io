var searchData=
[
  ['calcoefficient_5fread_0',['calcoefficient_read',['../class_b_n_o055_1_1_b_n_o055.html#a58e4c6f4675a21f870175baef5b4f186',1,'BNO055.BNO055.calcoefficient_read(self)'],['../class_b_n_o055_1_1_b_n_o055.html#a58e4c6f4675a21f870175baef5b4f186',1,'BNO055.BNO055.calcoefficient_read(self)']]],
  ['calcoefficient_5fwrite_1',['calcoefficient_write',['../class_b_n_o055_1_1_b_n_o055.html#a3cf3218278b6f8a29dc5ee6bc9e1e7be',1,'BNO055.BNO055.calcoefficient_write(self, coefficients)'],['../class_b_n_o055_1_1_b_n_o055.html#a3cf3218278b6f8a29dc5ee6bc9e1e7be',1,'BNO055.BNO055.calcoefficient_write(self, coefficients)']]],
  ['calib_5faddresses_2',['CALIB_ADDRESSES',['../class_b_n_o055_1_1_b_n_o055.html#af19399363fdf36952478ab49c8b8ac93',1,'BNO055::BNO055']]],
  ['calib_5fcoefficients_3',['CALIB_COEFFICIENTS',['../class_b_n_o055_1_1_b_n_o055.html#a9411164953c745766486b0f9d21a2dfc',1,'BNO055::BNO055']]],
  ['calib_5fstat_4',['CALIB_STAT',['../class_b_n_o055_1_1_b_n_o055.html#aad157d9c556c9148310d5defa997b2a1',1,'BNO055::BNO055']]],
  ['calstatus_5',['calstatus',['../class_b_n_o055_1_1_b_n_o055.html#a307860520f21fb0e26925288af695eab',1,'BNO055.BNO055.calstatus(self)'],['../class_b_n_o055_1_1_b_n_o055.html#a307860520f21fb0e26925288af695eab',1,'BNO055.BNO055.calstatus(self)']]],
  ['closedloop_6',['ClosedLoop',['../classclosedloop_1_1_closed_loop.html',1,'closedloop']]],
  ['closedloop_2epy_7',['closedloop.py',['../_lab_014_2closedloop_8py.html',1,'(Global Namespace)'],['../_lab_015_2closedloop_8py.html',1,'(Global Namespace)'],['../_lab_016_2closedloop_8py.html',1,'(Global Namespace)']]],
  ['ctask_8',['Ctask',['../_lab_014_2task_controller_8py.html#a6143c6810f955f67a340c757094e2b87',1,'taskController.Ctask(controller, K_p, K_i, K_d, reference, velocity, dutycycle, closedloopON, period)'],['../_lab_015_2task_controller_8py.html#a6e40339695497ab9d9ad86252d56bfe6',1,'taskController.Ctask(controller, K_p, K_i, K_d, reference, position, velocity, dutycycle, closedloopON, period)'],['../_lab_016_2task_controller_8py.html#a5bd450ea4913adb8cf1c5a4080466dbe',1,'taskController.Ctask(controllerOUTER, controllerINNER, K_pOUT, K_iOUT, K_dOUT, K_pIN, K_iIN, K_dIN, outerinvert, reference, ball_position, ball_velocity, ball_z, plat_position, plat_velocity, dutycycle, closedloopON, period)']]],
  ['currentcount_9',['currentcount',['../classencoder_1_1_encoder.html#a52333d1acfc5b6b25201f136951679da',1,'encoder::Encoder']]]
];
